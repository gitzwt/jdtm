(function(win,undefined){

    /*email suffix*/
    var suffixArr = new Array("@163.com", "@qq.com", "@126.com", "@hotmail.com", "@gmail.com", "@sohu.com", "@sina.com");

    /*public object YJ*/
    var YJ = YJ || {};
    YJ = {
        G : function(id){
            return typeof id == "string" ? document.getElementById(id) : id;
        },
        extend : function(destination,target){
            for( var prop in target ){
                destination[prop] = target[prop];
            }
            return destination;
        },
        getEvent : function(e){
            return e || window.event;
        },
        getTarget : function(e){
            return e.target || e.srcElement;
        },
        on : function(otarget,otype,fn){
            if( otarget.addEventListener ){
                otarget.addEventListener( otype,fn,false);
            }else if( otarget.attachEvent ){
                otarget.attachEvent( "on"+otype,function(){
                    fn.call(otarget);
                })
            }else{
                otarget["on"+otype] = fn;
            }
        },
        each : function(object, callback, args){
            var name, i = 0,
                length = object.length,
                isObj = length === undefined || (Object.prototype.toString.call(object) == "[object Function]");
            if(args) {
                if(isObj){
                    for (name in object){
                        if(callback.apply(object[ name ], args  === false )){ break; }
                    }
                }else{
                    for( ; i < length; ){
                        if( callback.apply( object[ i++ ], args ) === false ){ break; }
                    }
                }
            }else{
                if(isObj){
                    for (name in object){
                        if(callback.call(object[ name ], name, object[ name ]) === false){ break; }
                    }
                }else{
                    for( ; i < length; ) {
                        if( callback.call( object[ i ], i, object[ i++ ] ) === false){ break; }
                    }
                }
            }
            return object;
        },
        stopPropagaton : function(e){
            if(e.stopPropagaton){
                e.stopPropagation();
            }else{
                e.cancelBubble = true;
            }
        },
        getCharCode : function(ev){
            if( typeof ev.charCode == "number" ){
                return ev.charCode;
            }else{
                return ev.keyCode;
            }
        }
    }

    /*emailAutoComplete*/
    function emailAutoComplete(arg){
        this.opt = {
            subBox    : "ue_tip",
            tag       : "li",
            id        : "email",
            suffixArr : suffixArr ,
            hoverClass: "on"
        }
        this.setting = YJ.extend(this.opt,arg || {});
        this.curNum = 0;
        this.configValue = {
            _emailId : null,
            _subBoxId : null
        }
        if( !(this instanceof emailAutoComplete)){
            return new emailAutoComplete(arg);
        }
        this.init();
    }
    emailAutoComplete.prototype = {
        constructor : emailAutoComplete,
        tipBox      : function(v,obj){
            var that = this;
            that.display(that.configValue._subBoxId,1);
            var _str = '<span  id="j_utype" class="utype">请选择邮箱类型:</span><ul id="j_ulist">';
            _str += '<li><a href="javascript:void(0)"  class="cur_val">'+v+'</a></li>';
            var e = v.indexOf("@");
            if( e == -1 ){
                YJ.each( that.setting.suffixArr,function(k,o){
                    _str += '<li><a href="javascript:void(0)" id="e'+k+'">'+v+o+'</a></li>';
                })
            }else{
                var _sh = v.substring(0,e),
                    _se = v.substring(e);
                YJ.each( that.setting.suffixArr,function(k,o){
                    if( o.indexOf(_se)!=-1 ){
                        _str+='<li><a href="javascript:void(0)" id="e'+k+'">'+_sh+o+'</a></li>';
                    }
                })
            }
            _str +="</ul>";
            that.configValue._subBoxId.innerHTML = _str;
        },
        dropList : function(){
            var that = this;
            var _li = that.configValue._subBoxId.getElementsByTagName( that.setting.tag ),
                _len = _li.length;
            for( var i=_len-1;i>=0;i--){
                _li[i].className ="";
            }
            if( _len>1 ){
                if( that.curNum > _len-1 ){
                    that.curNum = 1;
                }
                if( that.curNum < 0 ){
                    that.curNum = _len-1;
                }
                _li[that.curNum].className = that.setting.hoverClass;
            }else{
                that.curNum = 0;
            }
        },
        display : function(obj,flag){

            if( flag == 1 ){
                obj.style.display = "block";
            }else{
                obj.style.display = "none";
            }
        },
        getClick : function(){
            var that = this;
            var _children = YJ.G("j_ulist").getElementsByTagName( 'li' );
            for( var i=_children.length-1;i>=0;i-- ){
                _children[i].onmouseover = (function(j){
                    return function(){
                        _children[j].className = that.setting.hoverClass;
                    }
                })(i);

                _children[i].onmouseout = (function(j){
                    return function(){
                        _children[j].className = "";
                    }
                })(i);

                _children[i].onclick = (function(j){
                    return function(e){
                        if( _children[j].id !="j_utype" ){
                            that.configValue._emailId.value = /<a[^\/].*>(.*)<\/a>/gi.exec(_children[j].innerHTML)[1];
                            that.display(that.configValue._subBoxId,0);

                            document.getElementById('inp_email').focus();
                            blur_email();
                            var nickElement = document.getElementById('nick');
                            if (nickElement) {
                                document.getElementById('nick').focus();
                                document.getElementById('nick').select();
                            }
                        }
                    }
                })(i);
            }
        },
        getKey : function(){
            var that = this;
            YJ.on(document,"keydown",function(e){
                var _ev = YJ.getEvent(e);
                switch( _ev.keyCode ){
                    case 40:
                        that.curNum++;
                        that.dropList();
                        break;
                    case 38:
                        that.curNum--;
                        that.dropList();
                        break;
                    default:
                        break;
                }
            });
            YJ.on(that.configValue._emailId,'keydown',function(e){
                var _li = that.configValue._subBoxId.getElementsByTagName( that.setting.tag ),
                    _ev = YJ.getEvent(e);
                if( _ev.keyCode  == 13 || _ev.keyCode == 9){
                    if (that.curNum > _li.length -1)
                        that.curNum = _li.length - 1;
                    this.value = /<a[^\/].*>(.*)<\/a>/gi.exec(_li[that.curNum].innerHTML)[1];
                    that.display(that.configValue._subBoxId,0);
                    that.curNum = 1;
                }
            });
        },
        init : function(){
            var that = this;
            that.configValue._emailId = YJ.G( that.setting.id ),
                that.configValue._subBoxId = YJ.G( that.setting.subBox );

            YJ.on(that.configValue._emailId,"keyup",function(e){
                var _ev = YJ.getEvent(e);

                if( _ev.keyCode == 40 || _ev.keyCode == 38 ){
                    return false;
                }
                if( this.value !="" ){
                    if( _ev.keyCode  != 38 && _ev.keyCode  != 40 && _ev.keyCode  !=13 && _ev.keyCode  !=27 ){
                        var _inputVal = this.value;
                        that.tipBox(_inputVal,this);
                        that.getClick();
                    }
                }else{
                    that.display(that.configValue._subBoxId,0);
                }
            });
            that.getKey();
            YJ.on(document,'click',function(){
                that.display(that.configValue._subBoxId,0);
            });
        }
    }
    win.emailAutoComplete = emailAutoComplete;
})(window);