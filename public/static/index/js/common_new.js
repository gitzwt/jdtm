var site_domain = document.location.href;
//通用弹窗
var nowDialogId,dialog_w,dialog_h;

function showWin(id){
    if(nowDialogId)
        closeWin();
    nowDialogId = id;
    dialog_w = $("#"+nowDialogId).width();
    dialog_h = $("#"+nowDialogId).height();
    var height = $(document).height();
    var w1 = $(window).width();
    var h1 = $(window).height();
    var sh = $(window).scrollTop();
    var str = '<div style="position:absolute;left:0;top:0;background:url(/static/home/images/mask.png) repeat;_background:url(about:blank);_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,sizingMethod=\'scale\', src=\'/static/home/images/mask.png\');width:100%; height:'+height+'px; z-index:1002;" id="pop_bg_win">';
    $("body").append(str);
//    $("#pop_bg_win").css("opacity","0.2");
    $("#"+nowDialogId).show().css({"top":((h1-dialog_h)/2+sh)+"px","left":(w1-dialog_w)/2,"zIndex":"1003","position":"absolute"});
    $(window).bind("resize", change_dialog);
    $(window).bind("scroll", change_dialog);
    return false;
}

function change_dialog(){
    var height = $(document).height();
    var w1 = $(window).width();
    var h1 = $(window).height();
    var sh = $(window).scrollTop();
    if(height != $('#pop_bg_win').height())
        $('#pop_bg_win').height(height);
    $("#"+nowDialogId).css({"top":((h1-dialog_h)/2+sh)+"px","left":(w1-dialog_w)/2,"zIndex":"1003","position":"absolute"});
    return false;
}

function closeWin(){
    $("#pop_bg_win").remove();
    $("#"+nowDialogId).hide();
    $(window).unbind("resize",change_dialog);
    $(window).unbind("scroll",change_dialog);
    nowDialogId = null;
    return false;
}

//右侧浮窗提示关闭或不再提醒
function CloseFloatOld(){
    $.cookie("Float","true",{path:"/",expires:1});
}
function CloseFloatOrder(){
    $.cookie("FloatOrder","true",{path:"/",expires:1});
}

//登陆注册弹窗
var log_pop_w,log_pop_h;
function log_pop(index) {
    log_pop_w = $("#login_p").width();
    log_pop_h = $("#login_p").height();
    var height = $(document).height();
    var w1 = $(window).width();
    var h1 = $(window).height();
    var sh = $(window).scrollTop();
    var str = '<div style="position:absolute;background:url(/static/home/images/mask.png) repeat;_background:url(about:blank);_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,sizingMethod=\'scale\', src=\'/static/home/images/mask.png\');left:0;top:0;width:100%;height:'+height+'px; z-index:999;" id="pop_bg">';
    $("body").append(str);
    //$("#pop_bg").css("opacity","0.3");
    $("#login_p").show().css({"top":((h1-log_pop_h)/2+sh)+"px","left":(w1-log_pop_w)/2,"zIndex":"1000"});
    $(window).bind("resize", change_log_pop);
    $(window).bind("scroll", change_log_pop);
    log_pop_tab(index);
    return false;
}
//关闭弹窗
var mobile_reg_click = 0;
var mail_reg_click = 0;
function close_log_pop(){
    $("#pop_bg").remove();
    $("#login_p").hide();
    $(window).unbind("resize",change_log_pop);
    $(window).unbind("scroll",change_log_pop);
    //初始化弹窗
    $('#login_p i').html('');
    $(':input','#login-win-form,#mobile-reg-win-form,#email-reg-win-form')
        .not(':button, :submit, :reset')
        .val('');
    $('input[name="login[username]"]').val($.cookie('U_M'));
    $('#login_p .submit_error').html('').hide();
    $('#btn_mobile_code').attr('disabled', true);
    mobile_reg_click = mail_reg_click = 0;
    return false;
}

function change_log_pop(){
    var height = $(document).height();
    var w1 = $(window).width();
    var h1 = $(window).height();
    var sh = $(window).scrollTop();
    if(height != $('#pop_bg').height())
        $('#pop_bg').height(height);
    $("#login_p").css({"top":((h1-log_pop_h)/2+sh)+"px","left":(w1-log_pop_w)/2,"zIndex":"1000"});
    return false;
}

function log_pop_tab(index) {
    switch (index) {
        case 'log':
            $('#login_p .log_li').addClass('on').siblings().removeClass('on');
            $('#login_p .c_log_con').show();
            $('#login_p .mobile_reg').hide();
            $('#login_p .mail_reg').hide();
            if ($('#login-code-li').length > 0) {   //刷新验证码
                changeImg('login_code');
            }
            break;
        case 'mobile_reg':
            $('#login_p .mobile_reg_li').addClass('on').siblings().removeClass('on');
            $('#login_p .reg_type').val(index);
            $('#login_p .c_log_con').hide();
            $('#login_p .mobile_reg').show();
            $('#login_p .mail_reg').hide();
            mobile_reg_click++;
            if (mobile_reg_click == 1)
                $('.inp_mobile').focus();
            break;
        case 'mail_reg':
            $('#login_p .mail_reg_li').addClass('on').siblings().removeClass('on');
            $('#login_p .reg_type').val(index);
            $('#login_p .c_log_con').hide();
            $('#login_p .mobile_reg').hide();
            $('#login_p .mail_reg').show();
            mail_reg_click++;
            if (mail_reg_click == 1)
                $('.inp_email').focus();
            if ($('#register-code-li').length > 0) {    //刷新验证码
                changeImg('register_code');
            }
            break;
    }
}
//ajax请求
function ajaxOperating(url,data,type,dataType){
    var callback=arguments[4]?arguments[4]:"callback";
    $.ajax({
        url:url,
        type:type,
        data:data,
        dataType: dataType,  //类型
        jsonp: 'callback', //jsonp回调参数，必须
        jsonpCallback:callback,
        error:function (){
            //alert("操作错误，请重试");
            //location.reload();
        },
        timeout: 10000
    })
}
//第三方登陆按扭hover事件
$('#third_login,#third_login_con').hover(
    function() {
        $('#third_login_con').show();
    },
    function() {
        $('#third_login_con').hide();
    }
);

$(function() {
    /*if($.cookie("NLRAF")==null){
     if(!$(".sc-wrap").text().length){
     var coll_html = '<div class="sc-wrap">';
     coll_html += '<div class="sc-topbar"><a class="sc-close" href="javascript:void(0);" onclick="CloseNLRAF(false)"></a><em></em><span>按&ensp;<b>Ctrl+D</b>，把一折特卖放入收藏夹，限时特价信息一手掌握！</span> <a href="javascript:void(0);" onclick="CloseNLRAF(true)">不再提醒</a></div>';
     coll_html += '</div>';
     $("body").prepend(coll_html);
     }
     }*/
    //关闭右侧浮窗
    if($.cookie("Float")){
        $('.scroll-banner_old').css('display','none');
    }else{
        $('.scroll-banner_old').css('display','block');
    }
    if($.cookie("FloatOrder")){
        $('.scroll-banner1').css('display','none');
    }else{
        $('.scroll-banner1').css('display','block');
    }
    $('input[name="login[username]"]').blur(function() {
        var val = $.trim($(this).val());
        if (!val) {
            $(this).siblings('i').html('*用户名不能为空');
        } else {
            $(this).siblings('i').html('');
        }
    });
    $('input[name="login[password]"]').blur(function() {
        var val = $.trim($(this).val());
        if (!val) {
            $(this).siblings('i').html('*密码不能为空');
        } else {
            $(this).siblings('i').html('');
        }
    });
    //手机注册完直接登录
    $('#reg_success_sure').click(function() {
        location.href = site_domain;
        return false;
    });
    //关闭手机注册成功提示
    $('#reg_success .close_1').click(function() {
        //恢复按扭
        var reg_type = $('#login_p .reg_type').val();    //注册类型
        var reg_btn_obj;
        if (reg_type == 'mail_reg') {
            reg_btn_obj = $('#mail_reg_btn');
        }  else {
            reg_btn_obj = $('#mobile_reg_btn');
        }
        reg_btn_obj.attr('disabled', false);
        //$.get('?m=ajax&ac=check&op=clear');
        closeWin();
        location.href = site_domain;
    });

    //关闭邮箱注册提示窗口
    $('#email_active_sure, #reg_success_2 .close_1').click(function() {
        closeWin();
        location.href = site_domain;
    });
    /*//关闭邮箱注册提示窗口
     $('#reg_success_third_sure, #reg_success_3 .close_1').click(function() {
     closeWin();
     //  location.href = 'www.1zhe.com';
     });*/
    //收藏商品
    /* $(document).delegate('.fav_h', 'click', function() {
     var $this = $(this);
     var gid = $this.attr('data-id');
     $.ajax({
     url: '?m=ajax&ac=operate&op=favorite',
     type: 'POST',
     dataType: 'json',
     data: {
     gid: gid
     },
     success: function(result) {
     if (result.code == -1) {
     log_pop('log');
     } else if (result.code == 0) {
     $this.removeClass('fav_h').addClass('fav');
     $this.attr('title', '点击取消收藏');
     message_tip(result.msg);
     } else {
     message_tip(result.msg, 3000);
     }
     }
     });
     });
     //取消收藏商品
     $(document).delegate('.fav', 'click', function() {
     var $this = $(this);
     var gid = $this.attr('data-id');
     $.ajax({
     url: '?m=ajax&ac=operate&op=delfavorite',
     type: 'POST',
     dataType: 'json',
     data: {
     gid: gid
     },
     success: function(result) {
     if (result.code == -2) {
     log_pop('log');
     } else if (result.code == 0) {
     $this.removeClass('fav').addClass('fav_h');
     $this.attr('title', '点击收藏');
     message_tip(result.msg);
     } else {
     message_tip(result.msg, 3000);
     }
     }
     });
     });*/
    $('#logout_btn').click(function() {
        $.ajax({
            url: '?m=ajax&ac=check&op=clear',
            type: 'GET',
            async: false,
            dataType: 'json',
            success: function(html) {
                var hash = document.location.hash;
                if (hash) {
                    site_domain = site_domain.replace(hash, '');
                }
                location.href = site_domain;
            },
            error: function() {
                location.href = 'http://'+document.location.host+'/?m=user&ac=logout';
            }
        });
    });
    //手机注册获取验证码
    var captcha_success = false;
    $('#btn_mobile_code').click(function() {
        var mobile = $('.inp_mobile').val();
        var $this = $(this);
        if ($.cookie('m_'+mobile)) {
            $this.siblings("i").html('请稍候再获取').css('color', '');
            return false;
        }
        if (!captcha_success) {
            showWin('mobile_reg_imgcode');
            changeImg('mobile_img_code');
            $('input[name="mobile_img_code"]').focus();
            return false;
        }
        $this.val('发送中...');
        $this.attr('disabled', true);
        //发送
        $.post('?m=ajax&ac=operate&op=mobile_code', {mobile: mobile}, function(result) {
            $this.val('免费获取手机验证码');
            if (result.success) {
                captcha_success = false;
                $this.siblings("i").html('已发送').css('color', 'green');
                btn_time($this.get(0));
                $.cookie('m_'+mobile, 1, {path:"/",expires:1/24/3600*55});
            } else {
                $this.siblings("i").html(result.err_msg).css('color', '');
                $this.removeAttr('disabled');
            }
        }, 'json');
    });
    $('#mobile_img_code_confirm').click(function() {
        var mobile_img_code_obj = $('input[name="mobile_img_code"]');
        var mobile_img_code = mobile_img_code_obj.val();
        if (!mobile_img_code) {
            mobile_img_code_obj.focus();
            $('#mobile_img_code_err').html('请输入验证码');
            return false;
        }
        $.ajax({
            url: '?m=ajax&ac=operate&op=mobile_img_code',
            type: 'POST',
            async: false,
            dataType: 'json',
            data:{mobile_img_code: mobile_img_code},
            success: function(result) {
                if (result.success) {
                    captcha_success = true;
                    closeWin();
                    $('#btn_mobile_code').click();
                } else {
                    changeImg('mobile_img_code');
                    mobile_img_code_obj.val('');
                    mobile_img_code_obj.focus();
                    $('#mobile_img_code_err').html('验证码错误,请重新输入');
                }
            }
        });
    });
    //手机绑定获取验证码
    $('#binding_mobile_code').click(function() {
        var mobile = $('#inp_mobile').val();
        var $this = $(this);
        $this.val('发送中...');
        $this.attr('disabled', true);
        //发送
        $.post('?m=ajax&ac=operate&op=mobile_code_binding', {mobile: mobile}, function(result) {
            $this.val('免费获取手机验证码');
            if (result.success) {
                $this.siblings("span").html('已发送').css('color', 'green');
                btn_time($this.get(0));
            } else {
                $this.siblings("span").html(result.err_msg).css('color', '');
                $this.removeAttr('disabled');
            }
        }, 'json');
    });
});
//登陆验证
function check_login() {
    var user_obj = $('input[name="login[username]"]');
    var pwd_obj = $('input[name="login[password]"]');
    var username = $.trim(user_obj.val());
    var password = pwd_obj.val();
    if(username=='' || username==null || typeof(username)=='undefined'){
        user_obj.focus();
        user_obj.siblings("i").html("*请填写用户名");
        return false;
    }
    if(password=='' || password==null || typeof(password)=='undefined'){
        pwd_obj.focus();
        pwd_obj.siblings("i").html("*请填写密码");
        return false;
    }
    //是否需要验证码
    var login_code = '';
    if ($('#login-code-li').length > 0) {
        login_code = $('input[name="login_code"]').val();
        if (!login_code) {
            $('.c_log_con .submit_error').show().html('请填写验证码');
            $('input[name="login_code"]').focus();
            return false;
        }
    }
    //提交
    var auto_login = $('input[name="autoLogin"]:checked').val();
    $('#login_go').attr('disabled', true);
    $('.c_log_con .submit_error').hide();
    ajaxOperating('?m=user&ac=login', {'username':username, 'password':password, 'auto_login': auto_login, 'login_code': login_code}, 'POST', 'jsonp', 'login');
    return false;
}
function login(json) {
    if (json.code == 0) {
        var hash = document.location.hash;
        if (hash) {
            site_domain = site_domain.replace(hash, '');
        }
        location.href = site_domain;
    } else {
        if (json.code == -3 || json.code == -1) {
            changeImg('login_code');
            $('input[name="login_code"]').val('');
        }
        $('.c_log_con .submit_error').show().html(json.msg);
        $('#login_go').attr('disabled', false);
        if (json.err_num >= 5 && $('#login-code-li').length <= 0) {
            var code_html = '<li id="login-code-li"><em>验证码：</em><input type="text" name="login_code" placeholder="请输入验证码" class="s_w2"><img id="login_code" src="?m=index&ac=validate" style="cursor:pointer;" title="看不清，换一张" onclick="changeImg(\'login_code\');"><span onclick="changeImg(\'login_code\');">看不清， 换一张</span></li>';
            $('.c_log_con ul').append(code_html);
        }
    }
}

//注册start
var mailFlg = true;
var MobileFlg = true;
//邮箱验证
function blur_email() {
//    var ue_tip = document.getElementById('ue_tip');
//    if(ue_tip.style.display == "block")
//        return false;
    var obj = $('.inp_email');
    var tip_obj = $('#blur_email_tip');
    //验证邮箱
    var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var val = $.trim(obj.val());
    if (!val) {
        tip_obj.html('*请填写常用邮箱');
        mailFlg = false;
        return false;
    }
    if (!re.test(val)) {
        tip_obj.html('邮箱格式不对哦');
        mailFlg = false;
        return false;
    }
    if (val.length < 6 || val.length > 30) {
        tip_obj.html('邮箱名：6-30个字符');
        mailFlg = false;
        return false;
    }
    ajaxOperating('?m=ajax&ac=check&op=email', {'email':val}, 'POST', 'jsonp', 'check_email');
    return true;
}
function check_email(json){
    var tip_obj = $('#blur_email_tip');
    if(json.code==0){//可以使用
        tip_obj.html('');
        mailFlg = true;
        return true;
    }else{
        tip_obj.html(json.msg);
        mailFlg = false;
        return false;
    }
}
//手机号码验证
function blur_mobile() {
    var obj = $('.inp_mobile');
    //验证手机号码
    var re = /^1[34578]\d{9}$/;
    var val = $.trim(obj.val());
    if (!val) {
        obj.siblings("i").html('*请填写手机号码');
        $('#btn_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
    if (!re.test(val)) {
        obj.siblings("i").html('手机号格式不对哦');
        $('#btn_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
    ajaxOperating('?m=ajax&ac=check&op=mobile', {'mobile':val}, 'POST', 'jsonp', 'check_mobile');
    return true;
}

function change_mobile(){
    var obj = $('.inp_mobile');
    //验证手机号码
    var re = /^1[34578]\d{9}$/;
    var val = $.trim(obj.val());
    if (!val) {
        obj.siblings("i").html('*请填写手机号码');
        $('#btn_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
    if (!re.test(val)) {
        obj.siblings("i").html('手机号格式不对哦');
        $('#btn_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }

    if(val.length == 11){
        ajaxOperating('?m=ajax&ac=check&op=mobile', {'mobile':val}, 'POST', 'jsonp', 'check_mobile');
    }
    return true;
}

function check_mobile(json) {
    var obj=$(".inp_mobile");
    if(json.code==0){//可以使用
        obj.siblings("i").html('');
        $('#btn_mobile_code').removeAttr('disabled');   //获取验证码按扭可用
        MobileFlg = true;
        return true;
    }else{
        obj.siblings("i").html(json.msg);
        $('#btn_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
}
//手机号码绑定验证
function blur_mobile_binding() {
    var obj = $('#inp_mobile');
    //验证手机号码
    var re = /^1[34578]\d{9}$/;
    var val = $.trim(obj.val());
    if (!val) {
        obj.siblings("span").html('*请填写手机号码');
        $('#binding_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
    if (!re.test(val)) {
        obj.siblings("span").html('手机号格式不对哦');
        $('#binding_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
    ajaxOperating('?m=ajax&ac=check&op=mobile', {'mobile':val}, 'POST', 'jsonp', 'check_mobile_binding');
    return true;
}
function check_mobile_binding(json) {
    var obj=$("#inp_mobile");
    if(json.code==0){//可以使用
        obj.siblings("span").html('');
        $('#binding_mobile_code').removeAttr('disabled');   //获取验证码按扭可用
        MobileFlg = true;
        return true;
    }else{
        obj.siblings("span").html(json.msg);
        $('#binding_mobile_code').attr('disabled', true);   //获取验证码按扭不可用
        MobileFlg = false;
        return false;
    }
}
//密码验证
function blur_pwd() {
    var reg_type = $('#login_p .reg_type').val();    //注册类型
    var obj;
    if (reg_type == 'mail_reg') {    //邮箱
        obj = $('input[name="reg[ma_password]"]');
    }
    if (reg_type == 'mobile_reg') {  //手机号
        obj = $('input[name="reg[mo_password]"]');
    }
    var pwd_val = obj.val();
    if(pwd_val.length < 6) {
        obj.siblings("i").html('密码不能少于6位');
        return false;
    } else if (pwd_val.length > 20) {
        obj.siblings("i").html('密码最长20位哦');
        return false;
    } else {
        obj.siblings("i").html('');
    }
    return true;
}
function blur_npwd(){
    var reg_type = $('#login_p .reg_type').val();    //注册类型
    var obj,obj_re;
    if (reg_type == 'mail_reg') {    //邮箱
        obj_re = $('input[name="reg[re_ma_password]"]');
        obj = $('input[name="reg[ma_password]"]');
    }
    if (reg_type == 'mobile_reg') {  //手机号
        obj_re = $('input[name="reg[re_mo_password]"]');
        obj = $('input[name="reg[mo_password]"]');
    }
    if (obj_re.val() != obj.val()) {
        obj_re.siblings("i").html('确认密码与密码不一致');
        return false;
    } else {
        obj_re.siblings("i").html('');
    }
    return true;
}
//验证用户协议
function check_agreement() {
    var reg_type = $('#login_p .reg_type').val();    //注册类型
    var obj;
    if (reg_type == 'mail_reg') {    //邮箱
        obj = $('.ma_agree');
    } else {  //手机号
        obj = $('.mo_agree');
    }
    if(!obj.is(":checked")) {
        obj.siblings("i").html('*需同意协议才能注册');
        return false;
    } else {
        obj.siblings("i").html('');
    }
    return true;
}
//注册验证
var bind_flag = false;
function check_reg() {
    var reg_type = $('#login_p .reg_type').val();    //注册类型
    var reg_name,reg_pwd,reg_re_pwd,reg_btn_obj;
    var reg_mobile_code = '';
    var register_code = '';
    if (reg_type == 'mail_reg') {
        if (!mailFlg) {
            $('.inp_email').focus();
            return false;
        }
        if (!blur_pwd()) {
            $('input[name="reg[ma_password]"]').focus();
            return false;
        }
        if (!blur_npwd()) {
            $('input[name="reg[re_ma_password]"]').focus();
            return false;
        }
        if (!check_agreement())
            return false;
        //是否需要验证码
        if ($('#register-code-li').length > 0) {
            register_code = $('input[name="register_code"]').val();
            if (!register_code) {
                $('.mail_reg .submit_error').show().html('请填写验证码');
                $('input[name="register_code"]').focus();
                return false;
            }
        }
        reg_name = $('.inp_email').val();
        reg_pwd = $('input[name="reg[ma_password]"]').val();
        reg_re_pwd = $('input[name="reg[re_ma_password]"]').val();
        reg_btn_obj = $('#mail_reg_btn');
    } else if (reg_type == 'mobile_reg') {
        if (!MobileFlg) {
            $('.inp_mobile').focus();
            return false;
        }
        if (!blur_pwd()) {
            $('input[name="reg[mo_password]"]').focus();
            return false;
        }
        if (!blur_npwd()) {
            $('input[name="reg[re_mo_password]"]').focus();
            return false;
        }
        if (!check_agreement())
            return false;
        reg_name = $('.inp_mobile').val();
        reg_pwd = $('input[name="reg[mo_password]"]').val();
        reg_re_pwd = $('input[name="reg[re_mo_password]"]').val();
        reg_mobile_code = $('input[name="reg_mobile_code"]').val();
        reg_btn_obj = $('#mobile_reg_btn');
        var error_tip_obj = $('.mobile_reg .submit_error');
        if (!bind_flag) {
            var reg_stop = 0;
            $.ajax({
                url: '?m=ajax&ac=operate&op=bind_register_check',
                type: 'POST',
                async: false,
                dataType: 'json',
                data:{mobile: reg_name,reg_mobile_code:reg_mobile_code},
                success: function(result) {
                    if (result.bind_status == 1) {
                        reg_stop = 1;
                        $('.user_type_span').html(result.user_type);
                        $('.nickname_span').html(result.nickname);
                        $('.mobile_span').html(reg_name);
                        $('#go_bind').attr('data-key', result.key);
                        $('#go_bind').attr('data-param', result.param);
                        showWin('bind_register_tip');
                    } else if (result.bind_status < 0) {
                        reg_stop = 1;
                        reg_btn_obj.attr('disabled', false);
                        if (result.bind_status == -2 || result.bind_status == -3) {
                            $('#btn_mobile_code').siblings("i").html(result.msg).css('color', '');
                        } else {
                            error_tip_obj.html(result.msg).show();
                        }
                    }
                }
            });
            if (reg_stop == 1) {
                return false;
            }
        }
    }
    //提交
    reg_btn_obj.attr('disabled', true);
    ajaxOperating('?m=user&ac=register', {'reg_name':reg_name, 'reg_pwd':reg_pwd, 'reg_re_pwd':reg_re_pwd, 'reg_mobile_code':reg_mobile_code,'register_code':register_code}, 'POST', 'jsonp', 'register');
    return false;
}
function register(json) {
    var reg_type = $('#login_p .reg_type').val();    //注册类型
    var reg_btn_obj, error_tip_obj;
    if (reg_type == 'mail_reg') {
        error_tip_obj = $('.mail_reg .submit_error');
        reg_btn_obj = $('#mail_reg_btn');
    }  else {
        error_tip_obj = $('.mobile_reg .submit_error');
        reg_btn_obj = $('#mobile_reg_btn');
    }
    if (json.code == 0) {
        close_log_pop();
        if (json.reg_type == 1) {
            showWin('reg_success');
        } else {
            if (json.reg_num >= 5 && $('#register-code-li').length <= 0) {
                var code_html = '<li id="register-code-li"><em>验证码：</em><input type="text" name="register_code" placeholder="请输入验证码" class="s_w2"><img id="register_code" src="?m=index&ac=validate" style="cursor:pointer;" title="看不清，换一张" onclick="changeImg(\'register_code\');"><span onclick="changeImg(\'register_code\');">看不清， 换一张</span></li>';
                $('.mail_reg ul').append(code_html);
            }
            $('#reg_success_2 span').html(json.reg_email);
            $('#email_active_sure').attr('href', 'http://'+json.go_url);
            reg_btn_obj.attr('disabled', false);
            showWin('reg_success_2');
        }
    } else {
        reg_btn_obj.attr('disabled', false);
        if (json.code == -5 || json.code == -6) {
            $('#btn_mobile_code').siblings("i").html(json.msg).css('color', '');
        } else {
            if (json.code == -7) {
                changeImg('register_code');
                $('input[name="register_code"]').val('');
            }
            error_tip_obj.html(json.msg).show();
        }
    }
}
$('#go_reg').click(function() {
    bind_flag = true;
    $('#mobile-reg-win-form').submit();
});
var go_bind_submit = 0;
$('#go_bind').click(function() {
    if (go_bind_submit > 0) return;
    var key = $(this).attr('data-key');
    var param = $(this).attr('data-param');
    if (key != null || param != null) {
        go_bind_submit++;
        var password = $('input[name="reg[mo_password]"]').val();
        var re_password = $('input[name="reg[re_mo_password]"]').val();
        $.ajax({
            url: '?m=ajax&ac=operate&&op=register_bind_mobile',
            type: 'POST',
            dataType: 'json',
            data: {
                key:key,
                param:param,
                password:password,
                re_password:re_password
            },
            success: function(result) {
                close_log_pop();
                if (result.code == 0) {
                    showWin('bind_register_success');
                } else {
                    alert(result.msg);
                    location.href = site_domain;
                }

            }
        });
    }
});
$('#bind_success_sure').click(function() {
    var hash = document.location.hash;
    if (hash) {
        site_domain = site_domain.replace(hash, '');
    }
    location.href = site_domain;
    return false;
});
//注册end

/*网站收藏代码*/
function addfavorite() {
    title = '一折特卖,9块9包邮,为您挑选天猫,淘宝性价比高的商品 - www.1zhe.com';
    url = 'http://www.1zhe.com';
    if(window.sidebar){
        try{
            window.sidebar.addPanel(title,url,'');
        }catch(e){
            alert("您的浏览器不支持该功能,请使用Ctrl+D收藏本页");
        }
    }else{
        try{
            window.external.AddFavorite(url,title);
        }catch(e){
            alert("您的浏览器不支持该功能,请使用Ctrl+D收藏本页");
        }
    }
}
/*网站保存到桌面代码*/
function toDesktop(sUrl,sName){
    try {
        var WshShell = new ActiveXObject("WScript.Shell");
        var oUrlLink = WshShell.CreateShortcut(WshShell.SpecialFolders("Desktop")     + "\\" + sName + ".url");
        oUrlLink.TargetPath = sUrl;
        oUrlLink.Save();
    }
    catch(e)  {
        alert("当前浏览器安全级别不允许操作！请设置后在操作！");
    }
}
//全选
function checkAll(cheobj, opobj) {
    var checked = cheobj.prop('checked');
    opobj.prop('checked', checked);
}
//全选的option操作
function checkoption(cheobj) {
    cheobj.prop('checked', false);
}

//消息提示
function message_tip(message, life) {
    var time = 1000;    //默认消失时间
    if (life) {
        time = life;
    }
    if ($("#tip_message").text().length > 0) {
        $("#tip_message").remove();

    }
    var msg = '<div id="tip_message" class="pop_2">' + message + '</div>';
    $('body').append(msg);
    var tip_div_h = $("#tip_message").height();
    var h1 = $(window).height();
    var sh = $(window).scrollTop();
    $("#tip_message").css({"top":((h1-tip_div_h)/2+sh)+"px","zIndex":"2000"});
    $("#tip_message").show(300).delay(time).fadeOut(300);
}

//刷新验证码
function changeImg(id){
    //document.getElementById(id).src="?m=index&ac=validate&nocache="+Math.random();
    $('#'+id).attr('src', '?m=index&ac=validate&nocache='+Math.random());
}

//验证是否是手机号或邮箱
function check_account(account) {
    var reg_mobile = /^1[34578]\d{9}$/;
    var reg_mail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (reg_mobile.test(account) || reg_mail.test(account)) {
        return true;
    } else {
        return false;
    }
}

//密码强度判断
var PasswordStrength = {LevelValue:[30,20,0], Factor:[1,2,5], KindFactor:[0,0,10,20], Regex:[/[a-zA-Z]/g,/\d/g,/[^a-zA-Z0-9]/g]};
PasswordStrength.StrengthValue = function(pwd) {
    var strengthValue = 0;
    var ComposedKind = 0;
    for(var i=0; i<this.Regex.length; i++){
        var chars = pwd.match(this.Regex[i]);
        if(chars != null){
            strengthValue += chars.length * this.Factor[i];
            ComposedKind ++;
        }
    }
    strengthValue += this.KindFactor[ComposedKind];
    return strengthValue;
};
//获取验证码倒计时按扭
var wait = 60;
function btn_time(btn) {
    if (wait == 0) {
        btn.removeAttribute("disabled");
        btn.value = "免费获取手机验证码";
        if (btn === $('#btn_mobile_code').get(0)) {
            $('#btn_mobile_code').siblings("i").html('');
        }
        wait = 60;
    } else {
        btn.setAttribute("disabled", true);
        btn.value = wait + "秒后重新获取";
        wait--;
        setTimeout(function () {
            btn_time(btn);
        }, 1000);
    }
}

function btn_time_bak(btn,id){
    if (wait == 0) {
        btn.removeAttribute("disabled");
        btn.value = "免费获取手机验证码";
        if (btn === $('#'+id).get(0)) {
            $('#'+id).next().html('');
        }
        wait = 60;
    } else {
        btn.setAttribute("disabled", true);
        btn.value = wait + "秒后重新获取";
        wait--;
        setTimeout(function () {
            btn_time_bak(btn,id);
        }, 1000);
    }
}

function btn_time_email(btn,id){
    if (wait == 0) {
        btn.removeAttribute("disabled");
        btn.value = "免费获取邮箱验证码";
        if (btn === $('#'+id).get(0)) {
            $('#'+id).next().html('');
        }
        wait = 60;
    } else {
        btn.setAttribute("disabled", true);
        btn.value = wait + "秒后重新获取";
        wait--;
        setTimeout(function () {
            btn_time_email(btn,id);
        }, 1000);
    }
}