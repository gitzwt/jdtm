<?php

namespace app\index\controller;

use app\index\Controller;
use think\Db;

class Cate extends Controller
{

    public function index()
    {
        //获取类别关键字key
        $keyword = $this->request->param('keyword');
        $page = $this->request->param('page');
        $this->view->assign(['title' => '京东'.$keyword, 'keyword' => $keyword, 'page' => $page,]);

        return $this->view->fetch();
    }

    public function getClassGoodsInfo()
    {
        $keyword = $this->request->param('keyword');
        $page = $this->request->param('page');
        $ids_array = self::goodsSearchByKey($keyword,$page)['Paragraph'];
        $goods = array();
        //获取该类别商品详情和价钱
        foreach ($ids_array as $k=>$val) {
            $details = self::goodsInfoBygoodsId($val['wareid']);
            $prices = self::goodsPriceBySkuId('J_'.$val['wareid']);
            $details['price'] = $prices['price'];
            $details['market_price'] = $prices['market_price'];
            $goods[$k] = $details;
        }
        return $goods;
    }

}
