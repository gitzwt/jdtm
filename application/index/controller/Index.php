<?php

namespace app\index\controller;

use app\index\Controller;
use think\Db;

class Index extends Controller
{

    public function index()
    {
        $this->view->assign('title', '京东特卖');

//        $goods = self::goodsSearchByKey('女装');
//        $goods = self::mobileGoodsInfoBySkuId('3648521');
//        print_r($goods);
//        exit();

        $goods = self::goodsInfoBygoodsId('2908765,2993065,4406804,2993067,3121127,3546458,3130716,3130682,2707453,3546946');
        //获取商品的原价/现价
        foreach ($goods as &$vo) {
            $vo['price'] = self::goodsPriceBySkuId('J_' . $vo['skuId'])['price'];
            $vo['market_price'] = self::goodsPriceBySkuId('J_' . $vo['skuId'])['market_price'];
        }

        //首页轮播图
        $banners = Db::name('Adv')->where('position_id', 1)->where('status', 1)->order('sort')->limit(4)->select();
        //单品优惠商品推荐
        $discount = Db::name('Adv')->where('position_id', 2)->where('status', 1)->order('sort')->limit(5)->select();

        $this->view->assign(['banners' => $banners, 'goods' => $goods, 'discount' => $discount,]);
        return $this->view->fetch();
    }

}
