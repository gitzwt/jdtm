<?php

namespace app\index\controller;

use app\index\Controller;

class Detail extends Controller
{

    public function index()
    {
        //获取单品id
        $id = $this->request->param('id');
        $good = self::goodsInfoBygoodsId($id)[0];   //单品详情
        $price = self::goodsPriceBySkuId('J_'.$id);
        //获取单品原价/现价
        $good['price'] = $price['price'];
        $good['market_price'] = $price['market_price'];
        $good_info = self::mobileGoodsInfoBySkuId($id); //单品移动页详情
        $good_class = self::classInfoByclassId(self::parseComma($good['category']))['product_sorts'];   //获取单品同类目标签

        $this->view->assign(['title' => $good['name'], 'good' => $good, 'good_class' => $good_class, 'good_info' => $good_info,]);
        return $this->view->fetch();
    }

    public function detail_class()
    {
        $id = $this->request->param('id');
        $ids_array = explode(',',self::parseComma(self::idsPriceById($id))[0]);     //获取单品同类目商品
        $goods = array();
        //获取同类商品详情和价钱
        foreach ($ids_array as $k=>$val) {
            $details = self::goodsInfoBygoodsId($val)[0];
            $prices = self::goodsPriceBySkuId('J_'.$val);
            $details['price'] = $prices['price'];
            $details['market_price'] = $prices['market_price'];
            $goods[$k] = $details;
        }
        return $goods;
    }

}
