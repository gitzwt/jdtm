<?php
namespace app\common\validate;

use think\Validate;

class Adv extends Validate
{
    protected $rule = [
        "title|广告标题" => "require",
        "position_id|广告展位" => "require",
        "pic|广告图片" => "require",
        "sort|广告排序" => "require",
        "start_time|开始时间" => "require",
        "end_time|结束时间" => "require",
        "status|广告状态" => "require",
    ];
}
