<?php
namespace app\common\validate;

use think\Validate;

class AdvPosition extends Validate
{
    protected $rule = [
        "name|名称" => "require",
        "code|代码" => "require",
        "status|状态" => "require",
    ];
}
