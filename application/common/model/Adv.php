<?php
namespace app\common\model;

use think\Model;

class Adv extends Model
{
    // 指定表名,不含前缀
    protected $name = 'adv';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    public function advPosition()
    {
        return $this->hasOne('AdvPosition','id','position_id')->field('name');
    }
}
