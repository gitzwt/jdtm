<?php
namespace app\admin\controller;

\think\Loader::import('controller/Controller', \think\Config::get('traits_path') , EXT);

use app\admin\Controller;
use think\Db;

class Adv extends Controller
{
    use \app\admin\traits\controller\Controller;
    // 方法黑名单
    protected static $blacklist = [];

    protected function filter(&$map)
    {
        if ($this->request->param("title")) {
            $map['title'] = ["like", "%" . $this->request->param("title") . "%"];
        }
    }

    /**
     * 在首页前
     */
    protected function beforeIndex()
    {
        //广告位
        $position_list = Db::name('AdvPosition')->select();
        $this->view->assign('position_list', $position_list);
    }

    /**
     * 在添加前
     */
    protected function beforeAdd()
    {
        //广告位
        $position_list = Db::name('AdvPosition')->select();
        $this->view->assign('position_list', $position_list);
    }

    /**
     * 在编辑前
     */
    protected function beforeEdit()
    {
        // 广告位
        $position_list = Db::name('AdvPosition')->select();
        $this->view->assign('position_list', $position_list);
    }

    /**
     * 在回收站前
     */
    public function beforeRecycleBin()
    {
        // 广告位
        $position_list = Db::name('AdvPosition')->select();
        $this->view->assign('position_list', $position_list);
    }
}
